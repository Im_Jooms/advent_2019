package solution

import (
	"fmt"
	"strconv"
)

func DoSomething(lines []string) {
	sum := 0
	for _, l := range lines {
		num, _ := strconv.Atoi(l)
		num /= 3
		sum += num - 2
	}
	fmt.Println(sum)
}
